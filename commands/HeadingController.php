<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\common\repositories\HeadingRepository;
use phpDocumentor\Reflection\DocBlock\Tags\Formatter\AlignFormatter;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HeadingController extends Controller
{
    /**
     * Method generates list of headings like assoc array
     */
    public function actionGenerateHeadingsList()
    {
        $headingRepository = new HeadingRepository();

        $headings = $headingRepository->getHeadings();

        $arrList = [];

        print_r($headings);

//        $level = 0;
//
//        foreach ($headings as $heading) {
//            $arrList[$level] = [
//                'id' => $heading['id'],
//                'name' => $heading['name'],
//                'subs' => [],
//            ];
//
//            $break = false;
//
//            $subHeadings = $headingRepository->getHeadings($heading['id']);
//
//            if(count($subHeadings)) {
//               foreach ($subHeadings as $subHeading) {
//                   $arrList[$level]['subs'][] = $subHeading[];
//               }
//            } else {
//                $break = true;
//            }
//
//            if($break) {
//                break;
//            }
//
//            $level++;
//        }
//
//        print_r($arrList);

    }

}
