Что необходимо 

Стандартный минимальный набор для веба
1) Web server (apache или nginx)
2) MySQL (После установки надо добавить свои конфигурации в файле config/db.php)
3) PHP 7.4
4) composer

Установка
1) git clone https://vardanyanvov@bitbucket.org/vardanyanvov/test_task.git
2) composer install
3) yii migrate(создается админ username = admin, password = 123456)
4) Добавляем Рубрики (/admin/heading/index)
5) Добавляем Новости (/admin/post/create)
6) Используется также jquery datatable для отображения результатов апи,
(АПИ по умолчанию выбирает либо первую рубрику в базе либо рубрику с идентификатором 1).Увидеть результаты можно по ссылке /
7) Также реализован дополнительный часть задачи /api/heading/get-headings