$( document ).ready(function() {
    if($('#ajax_post_data').length){
        const categoryId = $('#categoryId').val();
        $.ajax({
            url: 'http://yii2posts.im/api/post/get-posts?heading_id=' + categoryId,
            type: "GET",
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $('#ajax_post_data').DataTable( {
                    data: data  ,
                    columns: [
                        { title: "Id" },
                        { title: "Title" },
                        { title: "Description" },
                        { title: "Created at" }
                    ]
                } );
            },
            error: function (error) {
                alert('error; ' + eval(error));
            }
        });
    }
});

