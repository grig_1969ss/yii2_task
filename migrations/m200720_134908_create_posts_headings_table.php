<?php

use yii\db\Migration;

class m200720_134908_create_posts_headings_table extends Migration
{

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('posts_headings', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'heading_id' => $this->integer()->notNull(),
        ], $tableOptions);

        // creates index for column `post_id`
        $this->createIndex(
            'idx-posts_headings-post_id',
            'posts_headings',
            'post_id'
        );

        // add foreign key for table `posts`
        $this->addForeignKey(
            'fk-posts_headings-post_id',
            'posts_headings',
            'post_id',
            'posts',
            'id',
            'CASCADE'
        );

        // creates index for column `heading_id`
        $this->createIndex(
            'idx-posts_headings-heading_id',
            'posts_headings',
            'heading_id'
        );

        // add foreign key for table `heading`
        $this->addForeignKey(
            'fk-posts_headings-heading_id',
            'posts_headings',
            'heading_id',
            'headings',
            'id',
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropTable('posts_headings');
    }

}