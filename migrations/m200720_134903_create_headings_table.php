<?php

use yii\db\Migration;

class m200720_134903_create_headings_table extends Migration
{

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('headings', [
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string()->notNull()->comment('Имя'),
            'url' => $this->string()->defaultValue(null)->comment('Имя'),
            'created_at' => $this->dateTime()->notNull()->comment('Создано на'),
            'parent_id' => $this->integer()->defaultValue(null),
        ], $tableOptions);

        // creates index for column `parent_id`
        $this->createIndex(
            'headings_fk1',
            'headings',
            'parent_id'
        );

        // add foreign key for table `headings`
        $this->addForeignKey(
            'headings_fk1',
            'headings',
            'parent_id',
            'headings',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('headings');
    }

}